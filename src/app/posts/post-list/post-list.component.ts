import { PostsService } from './../posts.service';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { Post } from '../post.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {
  // posts = [
    //   {title: 'First post', content: 'This is the first post\'s content'},
    //   {title: 'Second post', content: 'This is the second post\'s content'},
    //   {title: 'Third post', content: 'This is the third post\'s content'},
    // ];

    @Input() posts: Post[] = [];
  private postsSub: Subscription;
  post: Post;

    constructor(public postsService: PostsService) {}

    ngOnInit() {
      this.postsService.getPosts();
      this.postsSub = this.postsService.getPostUpdatedListener().subscribe((posts: Post[]) => {
        this.posts = posts;
      });
    }

  onHeartClicked(post) {
    post.like = !post.like;
    if (post.like === true) {
      post.count++;
    }
    if (post.like === false) {
      post.count--;
    }
  }

  onDelete(postId: string) {
    this.postsService.deletePost(postId);
  }

    ngOnDestroy() {
      this.postsSub.unsubscribe();
    }
}
