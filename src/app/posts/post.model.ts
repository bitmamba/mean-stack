export interface Post {
  id: string;
  title: string;
  content: string;
  like: boolean;
  count: number;
}
