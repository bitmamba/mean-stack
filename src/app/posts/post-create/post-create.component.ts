import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PostsService } from './../posts.service';
import { NgForm } from '@angular/forms';

import { Post } from '../post.model';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent implements OnInit {
  enteredTitle = '';
  enteredContent = '';
  heartClicked: boolean;

  onAddPost(form: NgForm) {
    if (form.invalid) {
      return;
    }
    const post: Post = {
      id: null,
      title: form.value.title,
      content: form.value.content,
      like: false,
      count: null
    };
    this.postsService.addPost(form.value.title, form.value.content);
    form.resetForm();
  }

  constructor(public postsService: PostsService) {}

  ngOnInit() {}

}
