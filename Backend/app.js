const express=require('express');
const bodyParser = require('body-parser')

const mongoose = require('mongoose');

const Post = require('./models/post')

const app = express();

mongoose.connect('mongodb+srv://super:super71@cluster0-6g7w8.mongodb.net/mean-stack?retryWrites=true', {useNewUrlParser: true})
  .then(() => {
    console.log('Connected to database!');
  })
  .catch(() => {
    console.log('Connection failed!');
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Request-With, Content-Type, Accept'
    );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE, OPTIONS'
  );
  next();
});

// a middleware with no mount path; gets executed for every request to the app
// app.use(function (req, res, next) {
//   res.setHeader('charset', 'utf-8')
//   next();
// });

// POST, needs dependency body-parser $ npm install --save body-parser
app.post("/api/posts", (req, res, next) => {
  const post=new Post({
    title: req.body.title,
    content: req.body.content,
    like: req.body.like,
    count: req.body.count
  });
  post.save().then(createdPost => {
    res.status(201).json({
      message: "Post added successfully",
      postId: createdPost._id
    });
  });
});

app.get('/api/posts',(req, res, next) => {
  Post.find() /* .find is a method provided by mongoose */
  .then(documents => {
    res.status(200).json({
    message: 'Posts fetched successfully!',
    posts: documents
  });
  });
});

app.delete('/api/posts/:id', (req, res, next) => {
  Post.deleteOne({
    _id: req.params.id
  }).then(result => {
    console.log(result);
    console.log(req.params.id);
    res.status(200).json({message: 'Post deleted!'});
  });
});

module.exports=app;
