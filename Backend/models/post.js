const mongoose=require('mongoose');

const postSchema=mongoose.Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  like: { type: Boolean, required: true },
  count: { type: Number, required: false }
});

module.exports = mongoose.model('Post', postSchema);
